module.exports = function(grunt) {

    grunt.initConfig({
        copy: {
            project: {
                expand: true,
                cwd: '.',
                src: [
                    '**'
                    , '!Gruntfile.js'
                    , '!package.json'
                    , '!bower.json'
                    , '!data/**'
                    , '!docker_compose.yml'
                    , '!Dockerfile'
                    , '!*.log'
                    , '!*.sh'
                    , '!*.bat'
                    , '!node_modules'
                    , '!public/vendor'
                ],
                dest: 'dist'
            }
        } /* configurações da tarefa */,
        clean: {
            dist: {
                src: 'dist'
            }
        },
        usemin : {
            html: 'dist/public/**/*.html'
        },
        useminPrepare: {
            options: {
                root: 'dist/public',
                dest: 'dist/public'
            },html: 'dist/public/**/*.html'
        }
        , compress: {
            main: {
              options: {
                archive: 'dist/archive.zip'
                , level: 9
              },
              files: [
                {src: ['dist/public/js/*.*']} // includes files in path
              ]
            }
          }
    });

    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-usemin');
    grunt.loadNpmTasks('grunt-serve');
    grunt.loadNpmTasks('grunt-contrib-compress');

    grunt.registerTask('default', ['dist', 'minifica', 'compress']);
    grunt.registerTask('dist', ['clean', 'copy']);
    grunt.registerTask('minifica', ['useminPrepare', 'ngAnnotate', 'concat', 'uglify', 'cssmin', 'usemin']);
};
